## Interface: 11304
## Title: VKP
## Notes: Vendetta Kill Points
## Author: Wenlock@EU-Firemaw
## Version: @project-version@
## DefaultState: Enabled
## SavedVariables: VKPData
## X-Max-Interface: 11304
## X-Min-Interface: 11303
## X-Curse-Project-ID: 348086

libs\LibStub\LibStub.lua
libs\LibST\Core.lua
libs\AceGUI-3.0\AceGUI-3.0.xml
VKP.lua
VKP.xml
VKP_Standings.lua
VKP_Bidder.lua
VKP_Import.lua
VKP_Integrations.lua
