# Vendetta Kill Points

World of Warcraft Classic addon that let you import DKP values to officer note,
and have everyone in the guild read this DKP out in a structured way. It is also
integrated loosely with Classic Raid Tracker to provide some additional features.

## VKP Import
Imports line separated DKP into officer note at bulk. `/vkp import`

## VKP Standings
Displays vkp standidgs to raiders and officers. `/vkp mages` se all options by
using `/vkp help`

## VKP Bidder
Simple module that let players bid for items up to their maximum DKP. `/vkp bid`

## VKP Integrations (Classic Raid Tracker)
If present displays some extra features for users of this addon.
- Deduct DKP from the current Classic Raid Trakcer -raid from officer note. `/vkp current`
- Adding a button to sync an auction from "Bidder" to the current Classic Raid Tracker raid.

### Todo
- Make items linkable in bidder
