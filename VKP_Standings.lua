if (not VKP) then
    VKP = {}
end

function VKP_getPointsForCurrentPlayer()
    local playerName = UnitName("player")
    local wowtime, points, adjustment = VKP_getPointsForPlayer(playerName)
    return points
  end

  function VKP_getPointsPrClass(classes)
    local playersFound = {}
    local totalmembers = GetNumGuildMembers(true)
    for x = 1, totalmembers do
      local name, rank, rankIndex, level, class, zone, publicnote, officernote, online, status = GetGuildRosterInfo(x)
      local playerName = Ambiguate(name, "guild")
      local wowtime, points, adjustment = VKP_getPointsFromNote(officernote)
      local limitToRaid = not playerInRaid() or UnitInRaid(playerName)
      if points and points > 0 and inArray(classes, string.upper(class)) and limitToRaid then
        table.insert(playersFound, {playerName, class, points + adjustment})
      end
    end
    table.sort(
      playersFound,
      function(a, b)
        return (a[3] > b[3])
      end
    )
    local headline = "Points for"
    for i, class in pairs(classes) do
      headline = headline .. " " .. class
    end
    VKP_Print(headline)
    for i, value in ipairs(playersFound) do
      local name, class, points = unpack(value)
      local formatted = VKP_ReportLineFormatter(name, class, points)
      VKP_Print(formatted)
    end
  end

  local listener = CreateFrame("frame", nil, UiParent)

function stringWidth(testStr)
  if (not VKP.fontstring_len) then
    VKP.fontstring_len = listener:CreateFontString(nil, "background", "GameFontNormal")
  end
  local _, fontSize = FCF_GetChatWindowInfo(1)
  if (fontSize < 1) then
    fontSize = 10
  end
  local fonte, _, flags = VKP.fontstring_len:GetFont()
  VKP.fontstring_len:SetFont(DEFAULT_CHAT_FRAME:GetFont(), fontSize, flags)
  VKP.fontstring_len:SetText(testStr)
  return VKP.fontstring_len:GetStringWidth()
end

function VKP_ReportLineFormatter(playerName, class, points)
  local maxNameWidth = stringWidth("XXXXXXXXXXX")
  local maxValueWidth = stringWidth("9999")
  local dotWidth = stringWidth(".")
  local spaceWidth = stringWidth(" ")
  local nameWidth = stringWidth(playerName)
  local valueWidth = stringWidth(points)
  local openSpaceWidth = (maxNameWidth - nameWidth) + (maxValueWidth - valueWidth)
  local numbDots = round(openSpaceWidth / dotWidth)
  local classColor = RGBToHEX(RAID_CLASS_COLORS[string.upper(class)])
  return "\124c" .. classColor .. playerName .. "\124cffffff00" .. string.rep(".", numbDots) .. points
end

function capitalize(str)
  return str:sub(1, 1):upper() .. str:sub(2)
end

function VKP_PrintPoints(playerName, class, points)
  local classColor = RGBToHEX(RAID_CLASS_COLORS[string.upper(class)])
  print("|c" .. classColor .. playerName .. ": |cffffff00 " .. points .. " points")
end

function RGBToHEX(color)
  if (not color) then
    return "ffffffff"
  end
  local hex =
    string.format(
    "%.2X%.2X%.2X%.2X",
    (color.a or color.opacity or 1) * 255,
    (color.r or 0) * 255,
    (color.g or 0) * 255,
    (color.b or 0) * 255
  )
  return string.lower(hex)
end