if (not VKP) then
  VKP = {}
end
if VKPData == nil then
  VKPData = {}
end
VKP.guildMembers = {}
VKP.guildMemberNote = {}
VKP.bidderAddonMessagePrefix = "VKP_BIDDER"

function VKP_OnEvent(frame, event, prefix, msg, distribution, sender)
  if (event == "ADDON_LOADED") then
    if (prefix == "VKP") then
      VKP_Debug("Initializing VKP...")
      frame:UnregisterEvent("ADDON_LOADED")
      VKP_Bidder_Initialize(frame)
    end
  end
  if event == "CHAT_MSG_ADDON" and prefix == VKP.bidderAddonMessagePrefix then
    local playerName = Ambiguate(sender, "guild")
    VKP_Bidder_OnAddonMessage(msg, playerName, distribution)
  end
end

local sets = {
  ["DPS"] = {"HUNTER", "MAGE", "ROGUE", "WARLOCK", "WARRIOR"},
  ["CASTER"] = {"DRUID", "HUNTER", "MAGE", "PALADIN", "PRIEST", "SHAMAN", "WARLOCK"},
  ["HEALER"] = {"DRUID", "PALADIN", "PRIEST", "SHAMAN"},
  ["TANK"] = {"WARRIOR", "DRUID"},
  ["MELEE"] = {"WARRIOR", "HUNTER", "ROGUE"}
}

for i, set in pairs(sets) do
  for i, class in pairs(set) do
    sets[class] = {class}
  end
end

function VKP_OnLoad()
  VKPFrame:RegisterEvent("ADDON_LOADED")
  VKPFrame:RegisterEvent("GROUP_ROSTER_UPDATE")
  VKPFrame:RegisterEvent("CHAT_MSG_ADDON")
  SlashCmdList["VKP"] = VKP_Command
  SLASH_VKP1 = "/vkp"
  VKP_UpdateGuildMembers()
end

function VKP_Command(arg1)
  if not CanViewOfficerNote("player") then
    VKP_ErrorPrint("Requires read access to Officer Notes.  Please request this ability from your Guild Leader.")
  end
  VKP_UpdateGuildMembers()
  if arg1 then
    if sets[string.upper(arg1)] then
      VKP_getPointsPrClass(sets[string.upper(arg1)])
      return
    end
    local shortArg = arg1:sub(1, -2)
    if sets[string.upper(shortArg)] then
      VKP_getPointsPrClass(sets[string.upper(shortArg)])
      return
    end
  end

  if arg1 == "help" then
    for name, set in pairs(sets) do
      VKP_Print("/vkp " .. string.lower(name))
    end
  end

  if arg1 == "import" then
    if not CanEditOfficerNote() then
      return VKP_ErrorPrint("Unable to edit Officer Notes.")
    end
    return VKP_openImportBox()
  end

  if arg1 == "bid" then
    return VKP_openBidderBox()
  end

  if arg1 == "current" then
    if not CanEditOfficerNote() then
      return VKP_ErrorPrint("Unable to edit Officer Notes.")
    end
    VKP_SyncCurrentPointsFromTracker()
  end

  if arg1 == "" then
    VKP_UpdateGuildMembers()
    local playerName = UnitName("player")
    local wowtime, points, adjustment = VKP_getPointsForPlayer(playerName)
    if points then
      VKP_Print("You have " .. points + adjustment .. " vkp.")
    else
      VKP_Print("You have no points.")
    end
  end
end

function VKP_UpdateGuildMembers()
  local totalmembers = GetNumGuildMembers(true)
  if totalmembers == 0 then
    VKP_ErrorPrint("You are not in a guild")
    return
  end
  for x = 1, totalmembers do
    local name, rank, rankIndex, level, class, zone, publicnote, officernote, online, status = GetGuildRosterInfo(x)
    local playerName = Ambiguate(name, "guild")
    VKP.guildMembers[playerName] = x
    VKP.guildMemberNote[playerName] = officernote
  end
end

function VKP_playerIsInGuild(playerName)
  return VKP.guildMembers[playerName]
end

function VKP_Bidder_GetChannel()
  local channel = "GUILD"
  if playerInRaid() then
    channel = "RAID"
  end
  return channel
end

function VKP_Bidder_isAdmin()
  if playerInRaid() then
    return UnitIsGroupLeader("player") or UnitIsGroupAssistant("player")
  else
    return CanEditOfficerNote()
  end
end

function VKP_getPointsFromNote(note)
  if note then
    local wowtime, points, adjustment = strsplit(":", note)
    return tonumber(wowtime), tonumber(points), tonumber(adjustment)
  end
end

function VKP_getPointsForPlayer(playerName)
  local playerNote = VKP.guildMemberNote[playerName]
  return VKP_getPointsFromNote(playerNote)
end

function round(number, decimals)
  return (("%%.%df"):format(decimals)):format(number)
end

function toWowTime()
  local wowtime = (GetServerTime() - 1101164400) / 120
  return round(wowtime, 0)
end

function fromWowTime(wowtime)
  return (wowtime * 120) + 1101164400
end

function inArray(Arr, Val)
  if (type(Arr) ~= "table") then
    return false
  end
  for i, v in pairs(Arr) do
    if (v == Val) then
      return true
    end
  end
end

function VKP_isValidItemId(itemId)
  if not itemId then
    return false
  end
  local itemName, itemLink = GetItemInfo(itemId)
  if not itemName then
    return false
  end
  return true
end

function playerInRaid()
  return UnitInRaid(UnitName("player"))
end

function VKP_Debug(str)
  local playerName = UnitName("player")
  if playerName == "Wennylock" or playerName == "Finnegone"  then
    VKP_Print(str)
  end
end

function VKP_ErrorPrint(str)
  DEFAULT_CHAT_FRAME:AddMessage("[VKP-error] " .. tostring(str), 1.0, 0.5, 0.5)
end

function VKP_DebugPrint(str)
  if (true) then
    DEFAULT_CHAT_FRAME:AddMessage("[VKP-debug] " .. tostring(str), 0.75, 1.0, 0.25)
  end
end

function VKP_Print(str)
  DEFAULT_CHAT_FRAME:AddMessage("[VKP] " .. tostring(str), 1.0, 1.0, 0.5)
end

-- TRADE FUNCTIONS
function VKP_TradeItemById(itemId, receiverName)
  if not TradeFrame:IsVisible() then
    ClearCursor()
    local bag, slot = VKP_getContainerItemById(itemId)
    if bag ~= nil then
      PickupContainerItem(bag, slot)
      if CursorHasItem() then
        DropItemOnUnit(receiverName)
      end
    end
  end
end

function VKP_getContainerItemById(itemId)
  for bag = 0, NUM_BAG_SLOTS do
    for slot = 1, GetContainerNumSlots(bag) do
      local _, _, _, _, _, _, _, _, _, containerItemid = GetContainerItemInfo(bag, slot)
      if containerItemid == itemId then
        return bag, slot
      end
    end
  end
end

function vardump(t)
  if (type(t) ~= "table") then
    return
  end
  for a, b in pairs(t) do
    print(a, b)
  end
end
