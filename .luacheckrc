std = "lua51"
max_line_length = false

exclude_files = {
	"README.md",
	".luacheckrc",
    "libs",
    "release.sh",
}

ignore = {
	"11./SLASH_.*", -- Setting an undefined (Slash handler) global variable
	"11./BINDING_.*", -- Setting an undefined (Keybinding header) global variable
	"113/LE_.*", -- Accessing an undefined (Lua ENUM type) global variable
	"113/NUM_LE_.*", -- Accessing an undefined (Lua ENUM type) global variable
	"211", -- Unused local variable
	"211/L", -- Unused local variable "CL"
	"211/CL", -- Unused local variable "CL"
	"212", -- Unused argument
	"213", -- Unused loop variable
	-- "231", -- Set but never accessed
	"311", -- Value assigned to a local variable is unused
	"314", -- Value of a field in a table literal is unused
	"42.", -- Shadowing a local variable, an argument, a loop variable.
	"43.", -- Shadowing an upvalue, an upvalue argument, an upvalue loop variable.
	"542", -- An empty if branch
}

globals = {
    'bit.band',
    'ChatFrame_AddMessageEventFilter',
    'ClassicRaidTracker',
    'CloseMenus',
    'CreateFrame',
    'date',
    'DEFAULT_CHAT_FRAME',
    'FauxScrollFrame_GetOffset',
    'FauxScrollFrame_OnVerticalScroll',
    'FauxScrollFrame_Update',
    'floor',
    'GameFontDisableSmallLeft',
    'GameFontHighlightSmallLeft',
    'GameFontNormalSmallLeft',
    'GetAddOnMetadata',
    'GetBuildInfo',
    'GetCurrentMapAreaID',
    'GetCursorPosition',
    'GetCVar',
    'GetDefaultHighlight',
    'GetDefaultHighlightBlank',
    'GetDifficultyInfo',
    'geterrorhandler',
    'GetExpansionLevel',
    'GetGuildInfo',
    'GetGuildRosterInfo',
    'GetGuildRosterSelection',
    'GetGuildRosterShowOffline',
    'GetInstanceInfo',
    'GetItemIcon',
    'GetItemInfo',
    'GetLocale',
    'GetLootMethod',
    'GetMinimapShape',
    'getn',
    'GetNumGroupMembers',
    'GetNumGuildMembers',
    'GetRaidRosterInfo',
    'GetRealmName',
    'GetRealZoneText',
    'GetScreenHeight',
    'GetScreenWidth',
    'GetServerTime',
    'GRAY_FONT_COLOR',
    'gsub',
    'HIGHLIGHT_FONT_COLOR',
    'hooksecurefunc',
    'InterfaceOptions_AddCategory',
    'InterfaceOptionsFrame_OpenToCategory',
    'IsInRaid',
    'issecure',
    'ITEM_QUALITY0_DESC',
    'ITEM_QUALITY1_DESC',
    'ITEM_QUALITY2_DESC',
    'ITEM_QUALITY3_DESC',
    'ITEM_QUALITY4_DESC',
    'ITEM_QUALITY5_DESC',
    'ITEM_QUALITY6_DESC',
    'itemSubClass',
    'LibStub',
    'LOOT_ITEM',
    'LOOT_ITEM_MULTIPLE',
    'LOOT_ITEM_SELF',
    'LOOT_ITEM_SELF_MULTIPLE',
    'max',
    'MAX_PLAYER_LEVEL_TABLE',
    'Minimap',
    'NORMAL_FONT_COLOR',
    'OpenColorPicker',
    'PLAYER_DIFFICULTY1',
    'PLAYER_DIFFICULTY2',
    'PLAYER_DIFFICULTY5',
    'PLAYER_DIFFICULTY6',
    'PlaySound',
    'securecall',
    'SendChatMessage',
    'SetDefaultHighlight',
    'SetDefaultHighlightBlank',
    'SetGuildRosterSelection',
    'SetGuildRosterShowOffline',
    'SetRowHeight',
    'SlashCmdList',
    'SOUNDKIT',
    'StaticPopup_Hide',
    'StaticPopup_Show',
    'StaticPopupDialogs',
    'strlen',
    'strmatch',
    'strsplit',
    'strsub',
    'table.getn',
    'table.wipe',
    'tContains',
    'time',
    'tinsert',
    'tremove',
    'UIParent',
    'UnitAffectingCombat',
    'UnitName',
    'UnitRace',
    'UnitSex',
    'VIDEO_QUALITY_LABEL6',
    'wipe',
}