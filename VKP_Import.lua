if (not VKP) then
    VKP = {}
end
local AceGUI = LibStub("AceGUI-3.0")

function VKP_openImportBox()
    if not VKP.importFrame or not VKP.importFrame:IsShown() then
        local importContainer = AceGUI:Create("Frame")
        importContainer:SetTitle("Import VKP")
        importContainer:SetStatusText("Import dkp into officer note, this will destroy officer note!")
        importContainer:SetCallback(
            "OnClose",
            function(widget)
                AceGUI:Release(widget)
            end
        )
        importContainer:SetLayout("fill")
        local editbox = AceGUI:Create("MultiLineEditBox")
        editbox:SetLabel("Insert text:")
        editbox:SetWidth(400)
        editbox:SetHeight(400)
        editbox.frame:SetFrameStrata("FULLSCREEN")
        editbox:SetLabel("")
        editbox:SetCallback(
            "OnEnterPressed",
            function(widget)
                VKP_handleImportDkp(widget:GetText())
            end
        )
        importContainer:AddChild(editbox)
        VKP.importFrame = importContainer
    end
end

function VKP_handleImportDkp(text)
    VKP_Print("Starting to import VKP")
    VKP_UpdateGuildMembers()
    local vkps = {}
    for s in text:gmatch("[^\r\n]+") do
        local name, points = s:match("([^%s]+)%s+([%-]?%d+)")
        name = capitalize(name)
        if name and VKP_playerIsInGuild(name) then
            local wowtime, oldPoints, adjustment = VKP_getPointsForPlayer(name)
            if oldPoints == nil then
                oldPoints = 0
            end
            if tonumber(oldPoints) ~= tonumber(points) then
                VKP_createOfficerNote(VKP.guildMembers[name], points)
                VKP_Print("Updated points from " .. oldPoints .. " to " .. points .. " for " .. name .. ".")
            end
        end
    end
    VKP_Print("Done with import.")
end

function VKP_createOfficerNote(index, points, adjustment)
    if not points then
      points = 0
    end
    if not adjustment then
      adjustment = 0
    end
    local wowTime = toWowTime()
    local note = table.concat({toWowTime(), points, adjustment}, ":")
    GuildRosterSetOfficerNote(index, note)
  end

  function VKP_setPointsForPlayer(playerName, points, adjustment)
    local index = VKP.guildMembers[playerName]
    VKP_createOfficerNote(index, points, adjustment)
  end