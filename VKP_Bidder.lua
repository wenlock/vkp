if (not VKP) then
  VKP = {}
end
if VKPData == nil then
  VKPData = {}
end
local AceGUI = LibStub("AceGUI-3.0")
local ScrollingTable = LibStub("ScrollingTable")
local coolDownSeconds = 60
local biddingData = {}
local previousBid = {}
function biddingIsActive(lastBidServerTime)
  return GetServerTime() - lastBidServerTime < coolDownSeconds
end
function ShowIconInCell(rowFrame, cellFrame, data, cols, row, realrow, column, fShow, self, ...)
  if fShow then
    local itemId = self:GetCell(realrow, 1)
    local lastBidServerTime = self:GetCell(realrow, 6)
    local lastBidTime = GetTime() - (GetServerTime() - lastBidServerTime)
    local itemTexture = GetItemIcon(itemId)
    if not cellFrame.cellItemTexture then
      cellFrame.cellItemTexture = cellFrame:CreateTexture()
    end
    if not cellFrame.cellCooldown then
      cellFrame.cellCooldown = CreateFrame("Cooldown", "myCooldown", cellFrame, "CooldownFrameTemplate")
    end
    cellFrame.cellItemTexture:SetTexture(itemTexture)
    cellFrame.cellItemTexture:SetTexCoord(0, 1, 0, 1)
    cellFrame.cellItemTexture:Show()
    cellFrame.cellItemTexture:SetPoint("CENTER", cellFrame.cellItemTexture:GetParent(), "CENTER")
    cellFrame.cellItemTexture:SetWidth(25)
    cellFrame.cellItemTexture:SetHeight(25)
    if biddingIsActive(lastBidServerTime) then
      cellFrame.cellCooldown:SetAllPoints()
      cellFrame.cellCooldown:SetCooldown(lastBidTime, coolDownSeconds)
      cellFrame.cellCooldown:Show()
      cellFrame.cellItemTexture:SetDesaturated(0)
    else
      cellFrame.cellCooldown:Hide()
      cellFrame.cellItemTexture:SetDesaturated(1)
    end
    cellFrame:SetScript(
      "OnEnter",
      function()
        VKP_GUI_ItemTT:SetOwner(cellFrame, "ANCHOR_RIGHT")
        local _, itemLink = GetItemInfo(itemId)
        VKP_GUI_ItemTT:SetHyperlink(itemLink)
        VKP_GUI_ItemTT:Show()
      end
    )
    cellFrame:SetScript(
      "OnLeave",
      function()
        VKP_GUI_ItemTT:Hide()
        VKP_GUI_ItemTT:SetOwner(UIParent, "ANCHOR_NONE")
      end
    )
  end
end

function ShowLastBidderTooltip(rowFrame, cellFrame, data, cols, row, realrow, column, fShow, self, ...)
  if fShow then
    local itemId = self:GetCell(realrow, 1)
    cellFrame:SetScript(
      "OnEnter",
      function()
        if previousBid[itemId] ~= nil then
          VKP_GUI_ItemTT:SetOwner(cellFrame, "ANCHOR_CURSOR")
          VKP_GUI_ItemTT:AddLine(previousBid[itemId].bidder .. "   " .. previousBid[itemId].bid, 0.5, 0.5, 0.5)
          VKP_GUI_ItemTT:SetAlpha(0.1)
          VKP_GUI_ItemTT:Show()
        end
      end
    )
    cellFrame:SetScript(
      "OnLeave",
      function()
        VKP_GUI_ItemTT:Hide()
        VKP_GUI_ItemTT:SetOwner(UIParent, "ANCHOR_NONE")
      end
    )
  end
  self.DoCellUpdate(rowFrame, cellFrame, data, cols, row, realrow, column, fShow, self, ...)
  local lastBidServerTime = self:GetCell(realrow, 6)
  if biddingIsActive(lastBidServerTime) then
    cellFrame.text:SetTextColor(1, 1, 1, 1)
  else
    cellFrame.text:SetTextColor(0.5, 0.5, 0.5, 1)
  end
end
local VKP_BiddingTableDef = {
  {
    ["width"] = 25,
    ["DoCellUpdate"] = ShowIconInCell
  },
  {
    ["name"] = "Item",
    ["width"] = 65,
    ["defaultsort"] = "asc"
  },
  {
    ["name"] = "Who",
    ["width"] = 40,
    ["DoCellUpdate"] = ShowLastBidderTooltip
  },
  {
    ["name"] = "Bid",
    ["width"] = 40,
    ["align"] = "RIGHT",
    ["DoCellUpdate"] = ShowLastBidderTooltip
  },
  {
    ["name"] = "itemLink",
    ["width"] = 0
  },
  {
    ["name"] = "lastBid",
    ["width"] = 0
  }
}

-- https://github.com/hurricup/WoW-Ace3/blob/master/AceGUI-3.0/widgets/AceGUIContainer-Frame.lua

local VKP_Bidding_Table = nil
local VKP_Bidding_Bid_Buttons = {}
local VKP_Bidding_Remove_Button = nil
local VKP_Bidding_Trade_Button = nil
local VKP_Bidding_TableSelection = nil
local VKP_Bidding_Sync_Button = nil
local VKP_Bidding_Table_Ticker = nil

function VKP_createButton(tooltiptext)
  local button = AceGUI:Create("Button")
  button:SetText("-")
  button.text:SetPoint("TOPLEFT", 3, -1)
  button.text:SetPoint("BOTTOMRIGHT", -3, 1)
  button.text:SetFont("Fonts\\FRIZQT__.TTF", 9)
  if tooltiptext then
    button.frame:SetScript(
      "OnEnter",
      function()
        VKP_GUI_ItemTT:SetOwner(button.frame, "ANCHOR_TOPLEFT")
        VKP_GUI_ItemTT:AddLine(tooltiptext, 0.8, 0.8, 0.8)
        VKP_GUI_ItemTT:SetAlpha(0.1)
        VKP_GUI_ItemTT:Show()
      end
    )
    button.frame:SetScript(
      "OnLeave",
      function()
        VKP_GUI_ItemTT:Hide()
        VKP_GUI_ItemTT:SetOwner(UIParent, "ANCHOR_NONE")
      end
    )
  end
  return button
end

function VKP_createBiddingButton(bidFunction)
  local button = VKP_createButton()
  button:SetWidth(64)
  button:SetDisabled(true)
  button:SetCallback("OnClick", VKP_Bidding_Bid_ButtonOnClick)
  button.getBid = bidFunction
  return button
end

function VKP_Bidding_Table_Ticker_Function()
  local changed = false
  for i, v in pairs(biddingData) do
    if biddingData[i][7] and not biddingIsActive(biddingData[i][6]) then
      changed = true
      biddingData[i][7] = false
    end
  end
  if changed then
    VKP_Debug("Updating stuff when out...")
    VKP_Bidding_Table:SetData(biddingData, true)
    VKP_updateButtons()
  end
end

function VKP_openBidderBox()
  if not VKP.biddingFrame or not VKP.biddingFrame:IsShown() then
    VKP_UpdateGuildMembers()
    local targetWidth = 218
    local biddingContainer = AceGUI:Create("Window")
    biddingContainer:Show()
    biddingContainer:SetTitle("VKP Bidder")
    biddingContainer:SetLayout("Flow")
    biddingContainer:SetWidth(targetWidth)
    biddingContainer:SetHeight(200)
    biddingContainer:ClearAllPoints()
    biddingContainer:SetPoint(
      VKPData.winPoint[1],
      VKPData.winPoint[2],
      VKPData.winPoint[3],
      VKPData.winPoint[4],
      VKPData.winPoint[5]
    )
    biddingContainer:EnableResize(false)
    biddingContainer.frame:SetScript("OnUpdate", VKP_Bidding_TableOnUpdate)
    local tableContainer = AceGUI:Create("SimpleGroup")
    tableContainer:SetHeight(120)
    tableContainer:SetLayout("Flow")
    tableContainer:SetWidth(targetWidth)
    if not VKP_Bidding_Table then
      VKP_Bidding_Table = ScrollingTable:CreateST(VKP_BiddingTableDef, 4, 25, nil, tableContainer.frame)
    end
    if not VKP_Bidding_Table_Ticker then
      VKP_Bidding_Table_Ticker = C_Timer.NewTicker(1, VKP_Bidding_Table_Ticker_Function)
    end
    VKP_Bidding_Table.frame:SetFrameLevel(103)
    VKP_Bidding_Table.frame:SetPoint("TOPLEFT", tableContainer.frame, "TOPLEFT", -3, -15)
    VKP_Bidding_Table:EnableSelection(true)
    VKP_Bidding_Table:SetData(biddingData, true)
    biddingContainer:SetCallback(
      "OnClose",
      function(widget)
        local a, b, c, d, e = biddingContainer:GetPoint(biddingContainer:GetNumPoints())
        VKPData.winPoint = {a, nil, c, d, e}
        AceGUI:Release(widget)
        biddingContainer = nil
        VKP_Bidding_Table_Ticker:Cancel()
        VKP_Bidding_Table_Ticker = nil
      end
    )
    local buttonContainer = AceGUI:Create("SimpleGroup")
    buttonContainer:SetLayout("Flow")
    buttonContainer:SetFullWidth(true)
    buttonContainer:SetWidth(targetWidth)
    VKP_Bidding_Bid_Buttons[1] =
      VKP_createBiddingButton(
      function()
        return VKP_getSelectedBid() + 5
      end
    )
    VKP_Bidding_Bid_Buttons[2] =
      VKP_createBiddingButton(
      function()
        return VKP_getSelectedBid() + 50
      end
    )
    VKP_Bidding_Bid_Buttons[3] =
      VKP_createBiddingButton(
      function()
        local currentPlayerPoints = VKP_getPointsForCurrentPlayer()
        if currentPlayerPoints == nil then
          currentPlayerPoints = 100
        end
        return currentPlayerPoints
      end
    )
    for i, VKP_Bidding_Bid_Button in ipairs(VKP_Bidding_Bid_Buttons) do
      buttonContainer:AddChild(VKP_Bidding_Bid_Button)
    end

    biddingContainer:AddChild(tableContainer)
    biddingContainer:AddChild(buttonContainer)

    if VKP_Bidder_isAdmin() then
      biddingContainer:SetHeight(245)
      biddingContainer.frame:SetScript(
        "OnReceiveDrag",
        function()
          VKP_Debug("Drag received")
        end
      )
      local adminContainer = AceGUI:Create("SimpleGroup")
      adminContainer:SetLayout("Flow")
      adminContainer:SetWidth(targetWidth)
      local VKP_Bidder_AddItemEditBox = AceGUI:Create("EditBox")
      VKP_Bidder_AddItemEditBox:SetLabel("Add item")
      VKP_Bidder_AddItemEditBox:SetWidth(60)
      VKP_Bidder_AddItemEditBox:SetCallback(
        "OnEnterPressed",
        function(widget, event, text)
          VKP_SendAddItem(text)
          VKP_Bidder_AddItemEditBox:SetText(nil)
        end
      )
      adminContainer:AddChild(VKP_Bidder_AddItemEditBox)

      VKP_Bidding_Remove_Button = VKP_createButton("Remove auction")
      VKP_Bidding_Remove_Button:SetText("R")
      VKP_Bidding_Remove_Button:SetAutoWidth(true)
      VKP_Bidding_Remove_Button:SetDisabled(true)
      VKP_Bidding_Remove_Button:SetCallback("OnClick", VKP_Bidding_Remove_ButtonOnClick)
      adminContainer:AddChild(VKP_Bidding_Remove_Button)

      VKP_Bidding_Trade_Button = VKP_createButton("Trade item to winner")
      VKP_Bidding_Trade_Button:SetText("T")
      VKP_Bidding_Trade_Button:SetAutoWidth(true)
      VKP_Bidding_Trade_Button:SetDisabled(false)
      VKP_Bidding_Trade_Button:SetCallback("OnClick", VKP_Bidding_Trade_ButtonOnClick)
      adminContainer:AddChild(VKP_Bidding_Trade_Button)

      VKP_Bidding_Sync_Button = VKP_createButton("Sync item with MRT")
      VKP_Bidding_Sync_Button:SetText("S")
      VKP_Bidding_Sync_Button:SetAutoWidth(true)
      VKP_Bidding_Sync_Button:SetDisabled(false)
      VKP_Bidding_Sync_Button:SetCallback("OnClick", VKP_Bidding_Sync_ButtonOnClick)
      adminContainer:AddChild(VKP_Bidding_Sync_Button)

      biddingContainer:AddChild(adminContainer)
    end
    VKP_updateButtons()
    VKP.biddingFrame = biddingContainer
  end
end

function VKP_Bidding_Bid_ButtonOnClick(button)
  if button.getBid() > VKP_getSelectedBid() then
    VKP_SendBid(VKP_getSelectedItemId(), button.getBid())
  end
end
function VKP_Bidding_Sync_ButtonOnClick(button)
  if VKP_Bidding_TableSelection ~= nil then
    local bid = VKP_getSelectedBid()
    local bidder = VKP_getSelectedBidder()
    local itemId = VKP_getSelectedItemId()
    VKP_SendItemToTracker(itemId, bidder, bid)
  end
end

function VKP_Bidding_Remove_ButtonOnClick(button)
  if VKP_Bidding_TableSelection ~= nil then
    VKP_SendRemove(VKP_getSelectedItemId())
  end
end
-- and TradeFrameRecipientNameText:GetText() == selectedBidder
function VKP_Bidding_Trade_ButtonOnClick(button)
  if VKP_Bidding_TableSelection ~= nil then
    VKP_TradeItemById(VKP_getSelectedItemId(), VKP_getSelectedBidder())
  end
end

function VKP_getSelectedBid()
  return VKP_getSelected(4)
end

function VKP_getSelectedBidder()
  return VKP_getSelected(3)
end

function VKP_getSelectedIsActive()
  return VKP_getSelected(7)
end

function VKP_getSelectedItemId()
  return VKP_getSelected(1)
end

function VKP_getSelected(i)
  if VKP_Bidding_TableSelection ~= nil then
    return biddingData[VKP_Bidding_TableSelection][i]
  end
end

function VKP_Bidding_TableOnUpdate()
  local selection = VKP_Bidding_Table:GetSelection()
  if VKP_Bidding_TableSelection ~= selection then
    VKP_Bidding_TableSelection = selection
    VKP_updateButtons()
  end
end

function VKP_disableButton(button)
  button:SetDisabled(true)
end

function VKP_enableButton(button)
  local playerPoints = VKP_getPointsForCurrentPlayer()
  if playerPoints == nil then
    playerPoints = 100
  end
  button:SetDisabled(button.getBid() > playerPoints)
end

function VKP_updateButtons()
  for i, VKP_Bidding_Bid_Button in ipairs(VKP_Bidding_Bid_Buttons) do
    if VKP_Bidding_TableSelection ~= nil then
      VKP_Bidding_Bid_Button:SetText(VKP_Bidding_Bid_Button.getBid())
    end
    if
      VKP_Bidding_TableSelection == nil or (UnitName("player") == VKP_getSelectedBidder() and VKP_getSelectedBid() > 0) or
        not VKP_getSelectedIsActive()
     then
      VKP_disableButton(VKP_Bidding_Bid_Button)
    else
      VKP_enableButton(VKP_Bidding_Bid_Button)
    end
  end
  if VKP_Bidding_Remove_Button then
    VKP_Bidding_Remove_Button:SetDisabled(VKP_Bidding_TableSelection == nil)
  end
  if VKP_Bidding_Sync_Button then
    VKP_Bidding_Sync_Button:SetDisabled(VKP_Bidding_TableSelection == nil)
  end
  if VKP_Bidding_Trade_Button then
    VKP_Bidding_Trade_Button:SetDisabled(VKP_Bidding_TableSelection == nil)
  end
end

-- IO
local methods = {remove = "remove", bid = "bid", status = "status", restart = "restart"}
local receivers = {}

receivers[methods.remove] = function(sender, itemId, bid, bidTime)
  local itemName, itemLink = GetItemInfo(itemId)
  for i, v in pairs(biddingData) do
    if v[1] == itemId then
      VKP_Bidding_Table:ClearSelection()
      VKP_Bidding_TableSelection = nil
      tremove(biddingData, i)
      tremove(previousBid, itemId)
      VKP_Bidding_Table:SetData(biddingData, true)
      VKP_updateButtons()
    end
  end
end

receivers[methods.bid] = function(sender, itemId, bid, bidTime)
  local itemName, itemLink = GetItemInfo(itemId)
  local changed = false
  local shouldOpenBidBox = false
  local isNew = true
  local playerName = UnitName("player")
  if itemName == nil then
    return
  end
  for i, v in pairs(biddingData) do
    if v[1] == itemId then
      previousBid[itemId] = {
        bidder = biddingData[i][3],
        bid = biddingData[i][4]
      }
      isNew = false
      shouldOpenBidBox = (biddingData[i][3] == playerName)
      biddingData[i][2] = itemName
      biddingData[i][3] = sender
      biddingData[i][4] = tonumber(bid)
      biddingData[i][5] = itemLink
      biddingData[i][6] = tonumber(bidTime)
      biddingData[i][7] = biddingIsActive(tonumber(bidTime))
      changed = true
    end
  end
  if isNew then
    tinsert(
      biddingData,
      1,
      {
        tonumber(itemId),
        itemName,
        sender,
        tonumber(bid),
        itemLink,
        tonumber(bidTime),
        biddingIsActive(tonumber(bidTime))
      }
    )
    shouldOpenBidBox = true
  end
  if shouldOpenBidBox then
    VKP_Debug("Should open bid box.")
    VKP_openBidderBox()
  end
  if (changed or isNew) and VKP_Bidding_Table then
    VKP_Bidding_Table:SetData(biddingData, true)
    VKP_updateButtons()
  end
end

receivers[methods.restart] = function(sender, itemId, bid, bidTime)
  local playerName = UnitName("player")
  for i, v in pairs(biddingData) do
    if v[1] == itemId then
      biddingData[i][6] = bidTime
      VKP_Bidding_Table:SetData(biddingData, true)
      VKP_updateButtons()
    end
  end
end

receivers[methods.status] = function(sender, itemId, bid, bidTime)
  local playerName = UnitName("player")
  for i, v in pairs(biddingData) do
    if v[3] == playerName then
      VKP_SendBid(v[1], v[4], v[6])
    end
  end
end

function VKP_CreateAddonMessage(action, itemId, bid, bidTime)
  if bidTime == nil then
    bidTime = GetServerTime()
  end
  return table.concat(
    {
      action,
      itemId,
      bid,
      bidTime
    },
    ":"
  )
end

function VKP_SendBid(itemId, bid, bidTime)
  if not VKP_isValidItemId(itemId) then
    return
  end
  local msg = VKP_CreateAddonMessage(methods.bid, itemId, bid, bidTime)
  VKP_Bidder_SendAddonMessage(msg)
end

function VKP_SendRemove(itemId)
  if not VKP_isValidItemId(itemId) then
    return
  end
  local msg = VKP_CreateAddonMessage(methods.remove, itemId, 0)
  VKP_Bidder_SendAddonMessage(msg)
end

function VKP_SendRequestStatus()
  local msg = VKP_CreateAddonMessage(methods.status, 0, 0)
  VKP_Bidder_SendAddonMessage(msg)
end

function VKP_SendAddItem(itemlink)
  local _, itemLink = GetItemInfo(itemlink)
  local _, itemId = strsplit(":", itemlink)
  VKP_SendBid(itemId, 0)
end

function VKP_Bidder_Initialize()
  C_ChatInfo.RegisterAddonMessagePrefix(VKP.bidderAddonMessagePrefix)
  local msg = VKP_CreateAddonMessage(methods.status, 0, 0)
  VKP_Bidder_SendAddonMessage(msg)
  if VKPData.winPoint == nil then
    VKPData.winPoint = {"CENTER"}
  end
end

function VKP_Bidder_SendAddonMessage(msg)
  C_ChatInfo.SendAddonMessage(VKP.bidderAddonMessagePrefix, msg, VKP_Bidder_GetChannel())
end

function VKP_Bidder_OnAddonMessage(msg, sender, channel)
  if VKP_Bidder_GetChannel() == channel then
    VKP_Debug("VKP_Bidder_OnAddonMessage from " .. sender .. " = " .. msg)
    local action, itemId, bid, bidTime = strsplit(":", msg)
    local itemId = tonumber(itemId)
    if receivers[action] then
      receivers[action](sender, itemId, bid, bidTime)
    end
  end
end
