if (not VKP) then
    VKP = {}
end

function VKP_SyncCurrentPointsFromTracker()
    local numOfCurrentRaid = MRT_NumOfCurrentRaid
    if numOfCurrentRaid == nil then
        return
    end
    local adjustments = {}
    for playerName, index in pairs(VKP.guildMembers) do
        adjustments[playerName] = 0
    end

    for _, set in pairs(MRT_RaidLog[numOfCurrentRaid]["Loot"]) do
        local playerName = set["Looter"]
        if adjustments[playerName] then
            adjustments[playerName] = adjustments[playerName] + set["DKPValue"]
        end
    end

    for playerName, newAdjustment in pairs(adjustments) do
        local wowtime, oldPoints, oldAdjustment = VKP_getPointsForPlayer(playerName)
        if -newAdjustment ~= oldAdjustment then
            VKP_setPointsForPlayer(playerName, oldPoints, -newAdjustment)
            VKP_Print(playerName .. " was adjusted to " .. -newAdjustment)
        end
    end
end

function VKP_SendItemToTracker(ItemId, Looter, DKPValue)
    local raidnum = MRT_NumOfCurrentRaid
    if raidnum == nil then
        return
    end
    for i, v in ipairs(MRT_RaidLog[raidnum]["Loot"]) do
        if v["ItemId"] == ItemId then
            local itemName, itemLink = GetItemInfo(ItemId)
            VKP_Print("Updated Raid Tracker set " .. itemName .. " to " .. DKPValue .. " looted by " .. Looter)
            MRT_RaidLog[raidnum]["Loot"][i]["DKPValue"] = DKPValue
            MRT_RaidLog[raidnum]["Loot"][i]["Looter"] = Looter
            return
        end
    end
end
